/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/17 14:10:02 by sada-sil          #+#    #+#             */
/*   Updated: 2023/03/14 18:51:26 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SO_LONG_H
# define SO_LONG_H

# include "mlx/mlx.h"
# include "libft/libft.h"
# include <fcntl.h>

typedef struct s_count
{
	int		collectable;
	int		exit;
}	t_count;

typedef struct s_frame
{
	char	val;
	int		check;
}	t_frame;

typedef struct s_vars
{
	void	*mlx_ptr;
	void	*win_ptr;
	int		c_counter;
	int		steps;
	t_frame	**map;
}	t_vars;

//Utils
int		ft_line_len(char *line);
void	ft_free_arr(char ***s, int i);
void	ft_free_map(t_frame ***s);

//Check functions
int		ft_check_map(char **map, int line_amount);
int		ft_check_exit(char **map);
int		ft_check_item(char **map);
int		ft_check_start(char **map);
int		ft_check_walls(char **map, int line_amount);
int		ft_check_map_chars(char **map);
int		ft_check_path(t_frame **map);

//Ennemy
void	ft_move_wolf(t_vars *vars, int ox, int oy);
int		ft_count_wolves(t_frame **map);
int		ft_find_wolf(t_frame **map, int *x, int *y);

//Get Objects
void	ft_find_start(t_frame **map, int *x, int *y);
int		ft_find_player(t_frame **map, int *x, int *y);
void	ft_find_exit(t_vars *vars, int *x, int *y);
int		ft_count_collectables(t_frame **map);
int		ft_find_collectable(t_frame **map, int *x, int *y);

//Map
t_frame	**ft_set_map(char *file);
int		ft_get_map_line_amount(t_frame **map);
int		ft_get_map_frame_amount(t_frame **map);

//Game
void	ft_move(t_vars *vars, int direction);

//Events
int		destroy_event(void);
int		deal_key(int key, t_vars *vars);
int		update(t_vars *vars);

//MLX
int		create_window(t_frame **map);
void	*ft_get_image(void *mlx_ptr, char *path);

//Anim
void	ft_open_chest(t_vars *vars);
void	ft_move_key(t_vars *vars, int frame);

#endif