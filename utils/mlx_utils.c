/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/07 13:08:03 by sada-sil          #+#    #+#             */
/*   Updated: 2023/02/09 10:46:41 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

void	*ft_get_image(void *mlx_ptr, char *path)
{
	int		img_width;
	int		img_height;
	void	*img;

	img = mlx_png_file_to_image(mlx_ptr, path,
			&img_width, &img_height);
	return (img);
}
