/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   map_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/09 10:41:03 by sada-sil          #+#    #+#             */
/*   Updated: 2023/03/14 18:03:01 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

int	ft_get_map_frame_amount(t_frame **map)
{
	int	i;

	i = 0;
	while (map[0][i].val != 0)
		i++;
	return (i);
}

int	ft_get_map_line_amount(t_frame **map)
{
	int	i;

	i = 0;
	while (map[i] != NULL)
		i++;
	return (i);
}

int	ft_count_collectables(t_frame **map)
{
	int	count;
	int	x;
	int	y;

	y = -1;
	count = 0;
	while (map[++y] != NULL)
	{
		x = -1;
		while (map[y][++x].val != 0)
			if (map[y][x].val == 'C')
				count++;
	}
	return (count);
}

int	ft_count_wolves(t_frame **map)
{
	int	count;
	int	x;
	int	y;

	y = -1;
	count = 0;
	while (map[++y] != NULL)
	{
		x = -1;
		while (map[y][++x].val != 0)
			if (map[y][x].val == 'W')
				count++;
	}
	return (count);
}
