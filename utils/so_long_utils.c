/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   so_long_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 14:32:18 by sada-sil          #+#    #+#             */
/*   Updated: 2023/03/14 18:25:03 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

int	ft_line_len(char *line)
{
	int	i;

	i = 0;
	if (line == NULL)
		return (0);
	while (line[i] != '\0' && line[i] != '\n')
		i++;
	return (i);
}

void	ft_free_map(t_frame ***s)
{
	int	i;

	i = -1;
	while ((*s)[++i] != NULL)
		free((*s)[i]);
	free(*s);
}

void	ft_free_arr(char ***s, int i)
{
	while (--i >= 0)
	{
		free((*s)[i]);
		(*s)[i] = NULL;
	}
	free(*s);
}
