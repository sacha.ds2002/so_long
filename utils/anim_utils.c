/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   anim_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/09 10:39:11 by sada-sil          #+#    #+#             */
/*   Updated: 2023/02/10 14:37:18 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

void	ft_open_chest(t_vars *vars)
{
	int	x;
	int	y;

	ft_find_exit(vars, &x, &y);
	mlx_put_image_to_window(vars->mlx_ptr,
		vars->win_ptr, ft_get_image(vars->mlx_ptr,
			"./images/floor.png"), x * 64, y * 64);
	mlx_put_image_to_window(vars->mlx_ptr,
		vars->win_ptr, ft_get_image(vars->mlx_ptr,
			"./images/openChest.png"), x * 64, y * 64);
}

void	ft_move_key(t_vars *vars, int frame)
{
	int	x;
	int	y;

	x = -1;
	y = 0;
	while (ft_find_collectable(vars->map, &x, &y) != 0)
	{
		mlx_put_image_to_window(vars->mlx_ptr,
			vars->win_ptr, ft_get_image(vars->mlx_ptr,
				"./images/floor.png"), x * 64, y * 64);
		if ((frame / 4000) % 2 == 0)
		{
			mlx_put_image_to_window(vars->mlx_ptr,
				vars->win_ptr, ft_get_image(vars->mlx_ptr,
					"./images/collectable.png"), x * 64, y * 64 - 5);
		}
		else
		{
			mlx_put_image_to_window(vars->mlx_ptr,
				vars->win_ptr, ft_get_image(vars->mlx_ptr,
					"./images/collectable.png"), x * 64, y * 64 + 5);
		}
	}
}
