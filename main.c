/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/18 12:17:30 by sada-sil          #+#    #+#             */
/*   Updated: 2023/03/14 18:58:46 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "so_long.h"

int	main(int argc, char **argv)
{
	t_frame	**map;

	if (argc != 2)
		return (-1);
	map = ft_set_map(argv[1]);
	if (!map)
		return (-1);
	if (ft_check_path(map))
	{
		create_window(map);
		return (0);
	}
	else
	{
		ft_printf("Erreur\nInvalid map : No path through "
			"all the collectables and the exit is available.");
		ft_free_map(&map);
		return (-1);
	}
	return (0);
}
