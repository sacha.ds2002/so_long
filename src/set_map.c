/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_map.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/24 14:39:38 by sada-sil          #+#    #+#             */
/*   Updated: 2023/03/14 18:57:36 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

static int		ft_get_file_line_amount(char *file);
static t_frame	**ft_get_map_frames(char **tmp_map, int line_amount);

t_frame	**ft_set_map(char *file)
{
	char	**tmp_map;
	t_frame	**map;
	int		fd;
	int		i;
	int		line_amount;

	i = -1;
	line_amount = ft_get_file_line_amount(file);
	fd = open(file, O_RDONLY);
	if (fd < 0 || read(fd, 0, 0) < 0)
		return ((t_frame **) 0);
	tmp_map = (char **)malloc(sizeof(char *) * line_amount + 1);
	while (++i <= line_amount)
		tmp_map[i] = get_next_line(fd);
	close(fd);
	if (!ft_check_map(tmp_map, line_amount))
	{
		ft_free_arr(&tmp_map, line_amount);
		return ((t_frame **) 0);
	}
	map = ft_get_map_frames(tmp_map, line_amount);
	ft_free_arr(&tmp_map, line_amount);
	return (map);
}

static int	ft_get_file_line_amount(char *file)
{
	char	*tmp;
	int		fd;
	int		i;

	i = 0;
	fd = open(file, O_RDONLY);
	tmp = get_next_line(fd);
	while (tmp != NULL)
	{
		i++;
		free(tmp);
		tmp = get_next_line(fd);
	}
	free(tmp);
	close(fd);
	return (i);
}

int	ft_check_map(char **map, int line_amount)
{
	if (!ft_check_map_chars(map))
		ft_printf("Erreur\nInvalid map : The only possible"
			"chars are : '0' '1' 'C' 'E' 'P' 'W'.\n");
	else if (ft_check_exit(map) != 1)
		ft_printf("Erreur\nInvalid map : there should be one exit.\n");
	else if (ft_check_start(map) != 1)
		ft_printf("Erreur\nInvalid map : "
			"there should be one start position.\n");
	else if (ft_check_item(map) < 1)
		ft_printf("Erreur\nInvalid map : "
			"there should be at least one item.\n");
	else if (ft_check_walls(map, line_amount) == -1)
		ft_printf("Erreur\nInvalid map : "
			"the map should be rectangular and surrounded by walls\n");
	else
		return (1);
	return (0);
}

static t_frame	**ft_get_map_frames(char **tmp_map, int line_amount)
{
	t_frame	**map;
	int		len;
	int		i;
	int		j;
	int		fd;

	i = -1;
	len = ft_line_len(tmp_map[0]);
	map = (t_frame **)ft_calloc(sizeof(t_frame *), line_amount + 1);
	while (++i < line_amount)
	{
		j = -1;
		map[i] = (t_frame *)ft_calloc(sizeof(t_frame), len + 1);
		while (++j < len)
		{
			map[i][j].val = tmp_map[i][j];
			map[i][j].check = 0;
		}
	}
	return (map);
}
