/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_functions.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/25 13:33:03 by sada-sil          #+#    #+#             */
/*   Updated: 2023/03/14 18:02:30 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

int	ft_check_map_chars(char **map)
{
	int	i;
	int	j;

	i = -1;
	while (map[++i])
	{
		j = -1;
		while (map[i][++j] != '\n')
			if (!(map[i][j] == '0' || map[i][j] == '1' || map[i][j] == 'P'
				|| map[i][j] == 'C' || map[i][j] == 'E' || map[i][j] == 'W'))
				return (0);
	}
	return (1);
}

int	ft_check_walls(char **map, int line_amount)
{
	int	i;
	int	j;
	int	len;

	i = -1;
	len = ft_line_len(map[0]) - 1;
	while (++i < line_amount)
	{
		if (len != (ft_line_len(map[i]) - 1))
			return (-1);
		j = -1;
		if (i == 0 || i == line_amount - 1)
		{
			while (++j <= len)
				if (map[i][j] != '1')
					return (-1);
		}
		else
		{
			while (++j <= len)
				if ((j == 0 || j == len) && map[i][j] != '1')
					return (-1);
		}
	}
	return (0);
}

int	ft_check_exit(char **map)
{
	int	i;
	int	j;
	int	exit_amount;

	i = -1;
	exit_amount = 0;
	while (map[++i])
	{
		j = -1;
		while (map[i][++j] != '\n')
			if (map[i][j] == 'E')
				exit_amount++;
	}
	return (exit_amount);
}

int	ft_check_start(char **map)
{
	int	i;
	int	j;
	int	start_amount;

	i = -1;
	start_amount = 0;
	while (map[++i])
	{
		j = -1;
		while (map[i][++j] != '\n' && (map[i][j] == '0' || map[i][j] == '1'
			|| map[i][j] == 'P' || map[i][j] == 'C' || map[i][j] == 'E'))
			if (map[i][j] == 'P')
				start_amount++;
	}
	return (start_amount);
}

int	ft_check_item(char **map)
{
	int	i;
	int	j;
	int	item_amount;

	i = -1;
	item_amount = 0;
	while (map[++i])
	{
		j = -1;
		while (map[i][++j] != '\n')
			if (map[i][j] == 'C')
				item_amount++;
	}
	return (item_amount);
}
