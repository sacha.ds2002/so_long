/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_character.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/07 16:40:27 by sada-sil          #+#    #+#             */
/*   Updated: 2023/03/14 18:28:53 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

static int	ft_move_character(t_vars *vars, int x, int y, char *path)
{
	if (vars->map[y][x].val == 'W')
	{
		ft_printf("\n\nGNAP!\nVous vous êtes fait rongé jusqu'à l'os !\n\n");
		exit (0);
	}
	if (vars->map[y][x].val == 'C')
	{
		mlx_put_image_to_window(vars->mlx_ptr, vars->win_ptr,
			ft_get_image(vars->mlx_ptr, "./images/floor.png"),
			x * 64, y * 64);
		vars->c_counter--;
	}
	if (vars->map[y][x].val == 'E')
		if (vars->c_counter == 0)
			return (2);
	if (vars->map[y][x].val == '0' || vars->map[y][x].val == 'C')
	{
		mlx_put_image_to_window(vars->mlx_ptr, vars->win_ptr,
			ft_get_image(vars->mlx_ptr, path),
			x * 64, y * 64);
		vars->map[y][x].val = 'P';
		return (1);
	}
	return (0);
}

static int	ft_check_frame(t_vars *vars, int direction, int x, int y)
{
	char	*path;

	if (direction == 0)
	{
		path = "./images/characterA.png";
		x -= 1;
	}
	else if (direction == 1)
	{
		path = "./images/characterS.png";
		y += 1;
	}
	else if (direction == 2)
	{
		path = "./images/characterD.png";
		x += 1;
	}
	else
	{
		path = "./images/characterW.png";
		y -= 1;
	}
	return (ft_move_character(vars, x, y, path));
}

static void	ft_update_steps(t_vars *vars)
{
	char	*str;

	str = ft_itoa(vars->steps);
	mlx_put_image_to_window(vars->mlx_ptr,
		vars->win_ptr, ft_get_image(vars->mlx_ptr, "./images/wall.png"),
		64, 0);
	mlx_string_put(vars->mlx_ptr, vars->win_ptr, 74, 32, 0, str);
	free(str);
}

void	ft_move(t_vars *vars, int direction)
{
	int	x;
	int	y;
	int	ret;

	ft_find_player(vars->map, &x, &y);
	ret = ft_check_frame(vars, direction, x, y);
	if (ret == 1)
	{
		vars->map[y][x].val = '0';
		mlx_put_image_to_window(vars->mlx_ptr,
			vars->win_ptr, ft_get_image(vars->mlx_ptr, "./images/floor.png"),
			x * 64, y * 64);
		vars->steps++;
		ft_update_steps(vars);
	}
	else if (ret == 2)
	{
		vars->steps++;
		ft_printf("\n\nBravo ! \nVous avez terminé la carte en %d pas.\n\n",
			vars->steps);
		exit(0);
	}
}
