/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_objects.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/09 10:44:40 by sada-sil          #+#    #+#             */
/*   Updated: 2023/03/14 18:02:06 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

int	ft_find_player(t_frame **map, int *x, int *y)
{
	int	i;
	int	j;

	i = -1;
	while (map[++i] != NULL)
	{
		j = -1;
		while (map[i][++j].val != 0)
		{
			if (map[i][j].val == 'P')
			{
				*y = i;
				*x = j;
				return (0);
			}
		}
	}
	return (-1);
}

void	ft_find_exit(t_vars *vars, int *x, int *y)
{
	int	i;
	int	j;

	i = -1;
	while (vars->map[++i] != NULL)
	{
		j = -1;
		while (vars->map[i][++j].val != 0)
		{
			if (vars->map[i][j].val == 'E')
			{
				*x = j;
				*y = i;
				return ;
			}
		}
	}
}

void	ft_find_start(t_frame **map, int *x, int *y)
{
	int	i;
	int	j;

	i = 0;
	while (map[++i] != NULL)
	{
		j = 0;
		while (map[i][++j].val != 0)
		{
			if (map[i][j].val == 'P')
			{
				*y = i;
				*x = j;
				return ;
			}	
		}
	}
}

int	ft_find_collectable(t_frame **map, int *x, int *y)
{
	int	i;
	int	j;

	i = *y - 1;
	j = *x;
	while (map[++i] != NULL)
	{
		while (map[i][++j].val != 0)
		{
			if (map[i][j].val == 'C')
			{
				*y = i;
				*x = j;
				return (1);
			}	
		}
		j = -1;
	}
	return (0);
}

int	ft_find_wolf(t_frame **map, int *x, int *y)
{
	int	i;
	int	j;

	i = *y - 1;
	j = *x;
	while (map[++i] != NULL)
	{
		while (map[i][++j].val != 0)
		{
			if (map[i][j].val == 'W')
			{
				*y = i;
				*x = j;
				return (1);
			}	
		}
		j = -1;
	}
	return (0);
}
