/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ennemy_behaviour.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/10 10:08:59 by sada-sil          #+#    #+#             */
/*   Updated: 2023/03/14 18:05:31 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

static int	ft_update_position(t_vars *map, int x, int y);
static void	ft_clear_frame(t_vars *vars, int x, int y);
static int	*ft_random_moves(void);
static int	ft_execute_movement(t_vars *vars, int move, int x, int y);

void	ft_move_wolf(t_vars *vars, int x, int y)
{
	int	*move;
	int	i;

	if (ft_find_wolf(vars->map, &x, &y) != 0)
	{
		ft_move_wolf(vars, x, y);
		move = ft_random_moves();
		i = 0;
		while (i < 4 && ft_execute_movement(vars, move[i], x, y) == 0)
			i++;
		free(move);
	}
}

static int	ft_execute_movement(t_vars *vars, int move, int x, int y)
{
	int	ret;

	ret = 0;
	if (move == 0 && (vars->map[y][x + 1].val == '0'
		|| vars->map[y][x + 1].val == 'P'))
		ret = ft_update_position(vars, x + 1, y);
	else if (move == 1 && (vars->map[y + 1][x].val == '0'
		|| vars->map[y + 1][x].val == 'P'))
		ret = ft_update_position(vars, x, y + 1);
	else if (move == 2 && (vars->map[y][x - 1].val == '0'
		|| vars->map[y][x - 1].val == 'P'))
		ret = ft_update_position(vars, x - 1, y);
	else if (move == 3 && (vars->map[y - 1][x].val == '0'
		|| vars->map[y - 1][x].val == 'P'))
		ret = ft_update_position(vars, x, y - 1);
	if (ret == 1)
		ft_clear_frame(vars, x, y);
	return (ret);
}

static int	*ft_random_moves(void)
{
	int	*move;
	int	temp;
	int	i;
	int	j;

	move = (int *)ft_calloc(sizeof(int), 4);
	move[0] = 0;
	move[1] = 1;
	move[2] = 2;
	move[3] = 3;
	i = -1;
	while (++i < 4)
	{
		j = rand() % 4;
		temp = move[i];
		move[i] = move[j];
		move[j] = temp;
	}
	return (move);
}

static void	ft_clear_frame(t_vars *vars, int x, int y)
{
	vars->map[y][x].val = '0';
	mlx_put_image_to_window(vars->mlx_ptr,
		vars->win_ptr, ft_get_image(vars->mlx_ptr,
			"./images/floor.png"), x * 64, y * 64);
}

static int	ft_update_position(t_vars *vars, int x, int y)
{
	if (vars->map[y][x].val == 'P')
	{
		ft_printf("\n\nGNAP!\nVous vous êtes fait rongé jusqu'à l'os !\n\n");
		exit (0);
	}
	vars->map[y][x].val = 'W';
	mlx_put_image_to_window(vars->mlx_ptr,
		vars->win_ptr, ft_get_image(vars->mlx_ptr,
			"./images/wolf.png"), x * 64, y * 64);
	return (1);
}
