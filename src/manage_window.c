/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   manage_window.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/02 15:38:48 by sada-sil          #+#    #+#             */
/*   Updated: 2023/03/14 18:02:09 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

static void	place_objects(t_frame **map, void *mlx_ptr, void *win_ptr)
{
	int		x;
	int		y;

	y = -1;
	while (map[++y] != NULL)
	{
		x = -1;
		while (map[y][++x].val != 0)
		{
			if (map[y][x].val == 'P')
				mlx_put_image_to_window(mlx_ptr, win_ptr, ft_get_image(mlx_ptr,
						"./images/characterS.png"), x * 64, y * 64);
			else if (map[y][x].val == 'C')
				mlx_put_image_to_window(mlx_ptr, win_ptr, ft_get_image(mlx_ptr,
						"./images/collectable.png"), x * 64, y * 64);
			else if (map[y][x].val == 'E')
				mlx_put_image_to_window(mlx_ptr, win_ptr, ft_get_image(mlx_ptr,
						"./images/closedChest.png"), x * 64, y * 64);
			else if (map[y][x].val == 'W')
				mlx_put_image_to_window(mlx_ptr, win_ptr, ft_get_image(mlx_ptr,
						"./images/wolf.png"), x * 64, y * 64);
		}
	}
	mlx_string_put(mlx_ptr, win_ptr, 5, 32, 0, "Steps:  ");
}

static int	render_map(t_frame **map, void *mlx_ptr, void *win_ptr)
{
	void	*img_wall;
	void	*img_floor;
	int		x;
	int		y;

	img_floor = ft_get_image(mlx_ptr, "./images/floor.png");
	img_wall = ft_get_image(mlx_ptr, "./images/wall.png");
	y = -1;
	while (map[++y] != NULL)
	{
		x = -1;
		while (map[y][++x].val != 0)
		{
			mlx_put_image_to_window(mlx_ptr, win_ptr,
				img_floor, x * 64, y * 64);
			if (map[y][x].val == '1')
				mlx_put_image_to_window(mlx_ptr,
					win_ptr, img_wall, x * 64, y * 64);
		}
	}
	mlx_destroy_image(mlx_ptr, img_floor);
	mlx_destroy_image(mlx_ptr, img_wall);
	return (0);
}

int	create_window(t_frame **map)
{
	t_vars	vars;

	vars.mlx_ptr = mlx_init();
	vars.win_ptr = mlx_new_window(vars.mlx_ptr,
			ft_get_map_frame_amount(map) * 64,
			ft_get_map_line_amount(map) * 64, "so_long");
	vars.map = map;
	vars.steps = 0;
	vars.c_counter = ft_count_collectables(map);
	render_map(map, vars.mlx_ptr, vars.win_ptr);
	place_objects(map, vars.mlx_ptr, vars.win_ptr);
	mlx_string_put(vars.mlx_ptr, vars.win_ptr, 74, 32, 0, "0");
	mlx_key_hook(vars.win_ptr, deal_key, &vars);
	mlx_hook(vars.win_ptr, 17, 0, destroy_event, (void *)0);
	mlx_loop_hook(vars.mlx_ptr, update, &vars);
	mlx_loop(vars.mlx_ptr);
	return (1);
}
