/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_path.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/30 16:29:43 by sada-sil          #+#    #+#             */
/*   Updated: 2023/03/14 18:07:19 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

static int	ft_follow_path(t_frame **m, int y, int x, t_count *counter)
{
	int	r;

	if (m[y][x].val == 'C')
	{
		if (++(counter->collectable) == ft_count_collectables(m)
			&& counter->exit == 1)
			return (1);
	}
	else if (m[y][x].val == 'E')
	{
		counter->exit = 1;
		if (counter->collectable == ft_count_collectables(m))
			return (1);
	}
	r = 0;
	m[y][x].check = 1;
	if (m[y][x + 1].val != '1' && m[y][x + 1].check != 1 && r != 1)
		r = ft_follow_path(m, y, x + 1, counter);
	if (m[y + 1][x].val != '1' && m[y + 1][x].check != 1 && r != 1)
		r = ft_follow_path(m, y + 1, x, counter);
	if (x >= 1 && m[y][x - 1].val != '1' && m[y][x - 1].check != 1 && r != 1)
		r = ft_follow_path(m, y, x - 1, counter);
	if (y >= 1 && m[y - 1][x].val != '1' && m[y - 1][x].check != 1 && r != 1)
		r = ft_follow_path(m, y - 1, x, counter);
	return (r);
}

int	ft_check_path(t_frame **map)
{
	int		x;
	int		y;
	int		r;
	t_count	counter;

	counter.collectable = 0;
	counter.exit = 0;
	x = 0;
	y = 0;
	ft_find_start(map, &x, &y);
	r = ft_follow_path(map, y, x, &counter);
	return (r);
}
