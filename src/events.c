/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/02/02 11:44:41 by sada-sil          #+#    #+#             */
/*   Updated: 2023/03/14 18:50:52 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../so_long.h"

int	update(t_vars *vars)
{
	static int	frame = -1;

	frame++;
	if (frame % 4000 == 0)
	{
		if (ft_count_collectables(vars->map) != 0)
			ft_move_key(vars, frame);
	}
	if (frame % 6000 == 0)
	{
		if (ft_count_wolves(vars->map) > 0)
			ft_move_wolf(vars, -1, 0);
	}
	return (0);
}

int	destroy_event(void)
{
	exit(0);
}

int	deal_key(int key, t_vars *vars)
{
	if (key == 53)
		exit(0);
	else if (key == 0 || key == 1 || key == 2)
		ft_move(vars, key);
	else if (key == 13)
		ft_move(vars, 3);
	if (vars->c_counter == 0)
		ft_open_chest(vars);
	return (0);
}
