# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/01/17 14:10:06 by sada-sil          #+#    #+#              #
#    Updated: 2023/03/14 18:42:11 by sada-sil         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = so_long

CC = gcc
CFLAGS = -Wall -Werror -Wextra
RM = rm -rf
LD_FLAGS		=	libft/libft.a libmlx.dylib
MLX_FLAGS		=	-lm -lXext -lX11
SRCS 	= 	main.c \
		src/set_map.c \
		src/check_functions.c \
		src/check_path.c \
		src/events.c \
		src/manage_window.c \
		src/move_character.c \
		src/get_objects.c \
		src/ennemy_behaviour.c \
		utils/so_long_utils.c \
		utils/mlx_utils.c \
		utils/anim_utils.c \
		utils/map_utils.c

OBJS = $(SRCS:.c=.o)

${NAME}:	${OBJS}
	make libft.a -C libft
	make libmlx.dylib -C mlx
	cp mlx/libmlx.dylib ./libmlx.dylib
	${CC} ${CFLAGS} -Lmlx ${OBJS} -o ${NAME} ${LD_FLAGS} 

%.o: %.c
	gcc $(CCFLAGS) -Imlx -Iincludes -c $< -o $@

all:	${NAME}

clean:
	${RM} ${OBJS}
	$(MAKE) clean -C ./libft
	$(MAKE) clean -C ./mlx

fclean: clean
	${RM} libmlx.dylib
	${RM} ${NAME}
	$(MAKE) fclean -C ./libft

re: fclean all

.PHONY: all clean fclean re