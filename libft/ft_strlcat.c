/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/26 12:10:03 by sada-sil          #+#    #+#             */
/*   Updated: 2022/10/26 15:47:56 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

size_t	ft_strlcat(char *dst, const char *src, size_t dstsize)
{
	size_t	retvalue;
	size_t	dstlen;
	size_t	i;

	dstlen = ft_strlen(dst);
	i = 0;
	if (dstsize == 0)
		return (ft_strlen(src));
	else if (dstsize < dstlen)
		retvalue = ft_strlen(src) + dstsize;
	else
		retvalue = ft_strlen(src) + dstlen;
	while (src[i] && i + dstlen + 1 < dstsize)
	{
		dst[i + dstlen] = src[i];
		i++;
	}
	if (dstsize)
		dst[i + dstlen] = '\0';
	return (retvalue);
}
