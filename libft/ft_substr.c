/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sada-sil <sada-sil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/10/29 11:27:58 by sada-sil          #+#    #+#             */
/*   Updated: 2022/10/29 15:25:40 by sada-sil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*str;
	size_t	slen;

	slen = ft_strlen(s);
	if (slen < start)
		return (ft_strdup(""));
	slen -= start;
	if (len < slen)
		slen = len;
	str = (char *)malloc(sizeof(char) * (slen + 1));
	if (!str)
		return (0);
	ft_memcpy(str, s + start, slen);
	str[slen] = '\0';
	return (str);
}
